<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoMaquina extends Model
{
 	protected $table = 'tipo_maquina';
    protected $primaryKey = 'id_maquina';
}
