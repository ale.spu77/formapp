<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Cuenta extends Model
{
    protected $table = 'cuenta';
    protected $primaryKey = 'id_cuenta';
    protected $fillable = ['tx_nombre'];

    use SoftDeletes;
}
