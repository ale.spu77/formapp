<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cuenta;
use App\TipoMaquina;
use App\OrdenTrabajo;
use App\OrigenFalla;
use App\Tecnico;
use App\AvisoSegun;
use App\EstadoMaquina;
use App\TipoServicio;
use App\Preventivo;
use App\Chequeo;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    private function dateFormat($date){
        return date('Y-m-d',strtotime($date) );
    }
    public function saveForm(Request $request){

        $Orden = new OrdenTrabajo;

        $Orden->nu_periodo = $request->numero_periodo;
        $Orden->fecha      =  $this->dateFormat($request->fecha);
        $Orden->nu_iso_tools = $request->iso_tool; 
        $Orden->nu_ot      = $request->numero_ot;
        $Orden->id_cuenta  = $request->cuenta; 
        $Orden->id_tecnico = $request->tecnico; 
        $Orden->id_estado_maquina = $request->estado_maquina; 
        $Orden->inicio_trabajo  = $this->dateFormat($request->inicio_trabajo); 
        $Orden->hora_inicio     = $request->hora_inicio; 
        $Orden->fin_trabajo     = $this->dateFormat($request->fin_trabajo); 
        $Orden->hora_fin        = $request->hora_fin; 
        $Orden->dias_detencion  = $request->dias_detencion; 
        $Orden->horas_detencion = $request->horas_detencion; 
        $Orden->aplica_gasto    = $request->aplica_gasto; 
        $Orden->numero_maquina  = $request->numero_maquina; 
        $Orden->id_tipo_maquina = $request->tipo_maquina;
        $Orden->horometro       = $request->horometro; 
        $Orden->id_origen_falla = $request->origen_falla; 
        $Orden->id_aviso_segun  = $request->aviso_segun; 
        $Orden->id_tipo_servicio = $request->tipo_servicio; 
        $Orden->nu_presupuesto   = $request->presupuesto ;
        $Orden->monto_reparacion = $request->monto_reparacion; 
        $Orden->monto_arriendo   = $request->monto_reparacion; 
        $Orden->descripcion      = $request->observacion;
        $Orden->save();

        
        return response()->json($Orden->toArray());
    }
    public function saveFormPreventivo(Request $request){

        $Preventivo = new Preventivo;

        $Preventivo->fecha             =  $this->dateFormat($request->fecha);
        $Preventivo->nu_periodo        = $request->numero_periodo;
        $Preventivo->nu_pauta          = $request->nu_pauta;
        $Preventivo->id_cuenta         = $request->cuenta; 
        $Preventivo->id_tecnico        = $request->tecnico; 
        $Preventivo->id_estado_maquina = $request->estado_maquina; 
        $Preventivo->inicio_trabajo    = $this->dateFormat($request->inicio_trabajo); 
        $Preventivo->hora_inicio       = $request->hora_inicio; 
        $Preventivo->fin_trabajo       = $this->dateFormat($request->fin_trabajo); 
        $Preventivo->hora_fin          = $request->hora_fin; 
        $Preventivo->dias_detencion    = $request->dias_detencion; 
        $Preventivo->horas_detencion   = $request->horas_detencion; 
        $Preventivo->numero_maquina    = $request->numero_maquina; 
        $Preventivo->id_tipo_maquina   = $request->tipo_maquina;
        $Preventivo->horometro         = $request->horometro; 
        $Preventivo->id_tipo_servicio  = $request->tipo_servicio; 
        $Preventivo->descripcion       = $request->observacion;
        $Preventivo->save();

        $Chequeo = new Chequeo;

        $Chequeo->id_preventivo           = $Preventivo->id_preventivo;
        $Chequeo->inspeccion_visual       = $request->inspeccion_visual;
        $Chequeo->baterias_cables         = $request->baterias_cables;
        $Chequeo->limpieza_lubricacion    = $request->limpieza_lubricacion;
        $Chequeo->bomba_hidraulica        = $request->bomba_hidraulica;
        $Chequeo->unidad_traccion         = $request->unidad_traccion;
        $Chequeo->cables_potencia_control = $request->cables_potencia_control;
        $Chequeo->panel_contadores        = $request->panel_contadores;
        $Chequeo->frenos                  = $request->frenos;
        $Chequeo->direccion               = $request->direccion;
        $Chequeo->sistema_hidraulico      = $request->sistema_hidraulico;  
        $Chequeo->esamble_mastil          = $request->esamble_mastil;
        $Chequeo->chequeo_operacional     = $request->chequeo_operacional;
        $Chequeo->aditamientos            = $request->aditamientos;
        $Chequeo->equipo_limpieza         = $request->equipo_limpieza;
        $Chequeo->comentario              = $request->comentario;
        $Chequeo->save();

        return response()->json(true);

    }
    public function getFormulario(){

        $cuentas       = Cuenta::all()->pluck('tx_nombre','id_cuenta');
        $tipoMaquina   = TipoMaquina::all()->pluck('tx_nombre','id_maquina');
        $tecnicos      = Tecnico::all()->pluck('tx_nombre','id_tecnico');
        $OrigenFalla   = OrigenFalla::all()->pluck('tx_nombre','id_falla');
        $AvisoSegun    = AvisoSegun::all()->pluck('tx_nombre','id_aviso_segun');
        $EstadoMaquina = EstadoMaquina::all()->pluck('tx_nombre','id_estado_maquina');
        $TipoServicio  = TipoServicio::all()->pluck('tx_nombre','id_tipo_servicio');

      //  dump($EstadoMaquina);
        return view('formulario',compact(
                        'cuentas',
                        'tipoMaquina',
                        'tecnicos',
                        'OrigenFalla',
                        'AvisoSegun',
                        'EstadoMaquina',
                        'TipoServicio') 
                    );
    }
    public function getPreventivo(){

        $cuentas       = Cuenta::all()->pluck('tx_nombre','id_cuenta');
        $tipoMaquina   = TipoMaquina::all()->pluck('tx_nombre','id_maquina');
        $tecnicos      = Tecnico::all()->pluck('tx_nombre','id_tecnico');
        $OrigenFalla   = OrigenFalla::all()->pluck('tx_nombre','id_falla');
        $AvisoSegun    = AvisoSegun::all()->pluck('tx_nombre','id_aviso_segun');
        $EstadoMaquina = EstadoMaquina::all()->pluck('tx_nombre','id_estado_maquina');
        $TipoServicio  = TipoServicio::all()->pluck('tx_nombre','id_tipo_servicio');

      //  dump($EstadoMaquina);
        return view('preventivo',compact(
                        'cuentas',
                        'tipoMaquina',
                        'tecnicos',
                        'OrigenFalla',
                        'AvisoSegun',
                        'EstadoMaquina','TipoServicio') 
                    );
    }



    public function panel_preventivo(){   
        $cuentas     = Cuenta::all();
        $tipoMaquina = TipoMaquina::all();
        
        return view('panel_preventivo',compact('cuentas','tipoMaquina'));
    }   
    public function panel_correctivo(){   
        $cuentas     = Cuenta::all();
        $tipoMaquina = TipoMaquina::all();

        return view('panel_correctivo',compact('cuentas','tipoMaquina'));
    }

    public function loadFiltroPreventivo(Request $request){

        $Preventivo = Preventivo::with(
                                'aviso_segun',
                                'estado_maquina',
                                'tipo_servicio')->select();

        if ($request->ano) {
            $Preventivo->whereIn(DB::raw("year(fecha)"), $request->ano);
        }
        if ($request->periodo) {
            $Preventivo->whereIn('nu_periodo',$request->periodo);
        }
        if ($request->cuenta) {
            $Preventivo->whereIn('id_cuenta',$request->cuenta);
        }

        $Preventivo      = $Preventivo->get();
        $NumeroFallas    = $Preventivo->count();
        $HorasDetencion  = Carbon::createFromFormat('H:i:s','00:00:00');
        $HorasTotales    = Carbon::createFromFormat('H:i:s','00:00:00');
        $HorasTotalesMin = 0;
        
        foreach ($Preventivo as $key => $item) {
            $date_inicio  = Carbon::createFromTimestamp(strtotime($item->inicio_trabajo->toDateString().' '.$item->hora_inicio ));
            $date_fin     = Carbon::createFromTimestamp(strtotime($item->fin_trabajo->toDateString().' '.$item->hora_fin));
            $HorasTotalesMin = $HorasTotalesMin + $date_inicio->diffInMinutes($date_fin);
            list($hora,$minuto,$segundo) = explode(':', $item->horas_detencion);
            $HorasDetencion  = $HorasDetencion->addHours($hora)->addMinutes($minuto)->addSeconds($segundo);
        }

        $start = Carbon::createFromFormat('H:i:s','00:00:00');
        $HorasTotales        = $HorasTotales->addMinutes($HorasTotalesMin)->toTimeString();
        $HorasUtilizacionMin = $HorasDetencion->diffInMinutes($HorasTotales);
        if ($HorasTotalesMin) {
            $DisponibilidadTotal = number_format($HorasUtilizacionMin * 100 / $HorasTotalesMin, 2, ',', ' ');
        }else{
            $DisponibilidadTotal = 0;
          
        }
        $HorasDetencion   = $HorasDetencion->toTimeString();
        $HorasUtilizacion = Carbon::createFromFormat('H:i:s','00:00:00')->addMinutes($HorasUtilizacionMin);
        $HorasUtilizacion = $HorasUtilizacion->toTimeString();

        $Mantenimiento = ['Realizadas'=>11,'No Realizadas'=>11];
        $Turno         = ['Dia'=>11,'Noche'=>11];

        return response()->json(
                    compact(
                        'HorasUtilizacion',
                        'HorasDetencion',
                        'HorasTotales',
                        'DisponibilidadTotal',
                        'Mantenimiento','Turno')
                    );
    }

    public function loadFiltro(Request $request){

        $Correctivo = OrdenTrabajo::with(
                                'aviso_segun',
                                'estado_maquina',
                                'tipo_servicio',
                                'origen_falla')->select();

        if ($request->ano) {
            $Correctivo->whereIn(DB::raw("year(fecha)"), $request->ano);
        }
        if ($request->estado_maquina) {
            $Correctivo->whereIn('id_estado_maquina',$request->estado_maquina);
        }
        if ($request->origen_falla) {
            $Correctivo->whereIn('id_origen_falla',$request->origen_falla);
        }
        if ($request->periodo) {
            $Correctivo->whereIn('nu_periodo',$request->periodo);
        }
        if ($request->cuenta) {
            $Correctivo->whereIn('id_cuenta',$request->cuenta);
        }
        if ($request->tipo_maquina) {
            $Correctivo->whereIn('id_tipo_maquina',$request->tipo_maquina);
        }

        $Correctivo      = $Correctivo->get();
        $NumeroFallas    = $Correctivo->count();
        $HorasDetencion  = Carbon::createFromFormat('H:i:s','00:00:00');
        $HorasTotales    = Carbon::createFromFormat('H:i:s','00:00:00');
        $MontoReparacion = 0;
        $MontoArriendo   = 0;
        $HorasTotalesMin = 0;

        //$Correctivo->map(function($item,$key)use($HorasDetencion,$HorasTotales,$MontoReparacion){
        
        foreach ($Correctivo as $key => $item) {
            $date_inicio  = Carbon::createFromTimestamp(strtotime($item->inicio_trabajo->toDateString().' '.$item->hora_inicio ));
            $date_fin     = Carbon::createFromTimestamp(strtotime($item->fin_trabajo->toDateString().' '.$item->hora_fin));
            $HorasTotalesMin = $HorasTotalesMin + $date_inicio->diffInMinutes($date_fin);
            //$HorasTotales->addRealHours($diffInHours)->format('H:i:s');
            list($hora,$minuto,$segundo) = explode(':', $item->horas_detencion);
            $HorasDetencion  = $HorasDetencion->addHours($hora)->addMinutes($minuto)->addSeconds($segundo);
            $MontoReparacion = $MontoReparacion + intval($item->monto_reparacion);
            $MontoArriendo   = $MontoArriendo + intval($item->monto_arriendo);
        }
        //});
        $start = Carbon::createFromFormat('H:i:s','00:00:00');
        $HorasTotales        = $HorasTotales->addMinutes($HorasTotalesMin)->toTimeString();
        $HorasUtilizacionMin = $HorasDetencion->diffInMinutes($HorasTotales);
        $HorasDetencionMin   = $HorasDetencion->diffInMinutes($start);
        if ($HorasTotalesMin) {
            $DisponibilidadTotal = number_format($HorasUtilizacionMin * 100 / $HorasTotalesMin, 2, ',', ' ');
            $MTBF = (intval($HorasTotalesMin) / intval($NumeroFallas));
            $MTTR = (intval($HorasDetencionMin) / intval($NumeroFallas));
            $MTBF = Carbon::createFromFormat('H:i:s','00:00:00')->addMinutes($MTBF)->toTimeString();
            $MTTR = Carbon::createFromFormat('H:i:s','00:00:00')->addMinutes($MTTR)->toTimeString();
        }else{
            $DisponibilidadTotal = 0;
            $MTBF = 0;
            $MTTR = 0;
        }
        $HorasDetencion   = $HorasDetencion->toTimeString();
        $HorasUtilizacion = Carbon::createFromFormat('H:i:s','00:00:00')->addMinutes($HorasUtilizacionMin);
        $HorasUtilizacion = $HorasUtilizacion->toTimeString();

        if ($MontoReparacion && $MontoArriendo) {
            $CoeficienteMantenimiento = number_format(($MontoReparacion / $MontoArriendo) , 2, ',', ' '); 
        }else{
            $CoeficienteMantenimiento = 0;
        }

        $AccionMantenimiento  = $Correctivo->groupBy('aviso_segun.tx_nombre')->map(function($item,$key){
                                    return $item->count();
                                });
        $EstadoMaquina        = $Correctivo->where('aplica_gasto',true)
                                ->groupBy('estado_maquina.tx_nombre')->map(function($item,$key){
                                    return $item->sum('monto_reparacion');
                                });

        $TipoServicio         = $Correctivo->groupBy('tipo_servicio.tx_nombre')->map(function($item,$key){
                                    return $item->count();
                                });

        $OrigenFalla          = $Correctivo->groupBy('origen_falla.tx_nombre')->map(function($item,$key){
                                    return $item->count();
                                });


        //$AccionMantenimiento = $this->dataSet($AccionMantenimiento);

        return response()->json(
                    compact(
                        'HorasDetencion',
                        'HorasTotales',
                        'MontoReparacion',
                        'MontoArriendo',
                        'HorasUtilizacion',
                        'DisponibilidadTotal',
                        'MTBF',
                        'MTTR',
                        'CoeficienteMantenimiento',
                        'AccionMantenimiento',
                        'EstadoMaquina','TipoServicio','NumeroFallas','OrigenFalla')
                    );
    }

    public function dataSet($datas){

        $dataSet = [];
        $arroLevelColors = ['rgba(255, 99, 132, 0.2)', 'rgba(54, 162, 235, 0.2)', 'rgba(255, 206, 86, 0.2)'];
        $key = 0;

        foreach ($datas as $label => $value) {
        $data = $datas->Map(function($item,$key2)use($key){
                    return null;
                })->flatten();
            $data[$key] = $value;
            $dataSet[] = [
                'label'=>$label,
                'data' =>$data,
                'backgroundColor'=>$arroLevelColors[$key],
                'borderColor'=>$arroLevelColors[$key],
                'borderWidth'=>2,
                'pointStyle'=>'triangle',
            ];
            $key =$key+1;
        }
        // dd($dataSet);
        return ['datasets'=>$dataSet,'labels'=>$datas->keys()->toArray()];
    }


}
