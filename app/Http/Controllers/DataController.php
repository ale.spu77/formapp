<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cuenta;
use App\Cliente;
use App\TipoMaquina;
use App\OrdenTrabajo;
use App\OrigenFalla;
use App\Tecnico;
use App\AvisoSegun;
use App\EstadoMaquina;
use App\TipoServicio;   
use App\Preventivo;
use App\Chequeo;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use App\User;

class DataController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function editChequeo($chequeo){
        $check = [1=>'OK',2=>'AJUSTE',3=>'REPARAR',4=>'URGENTE',];
        foreach ($chequeo->toArray() as $key => $value) {
            if ( $key !== 'created_at' &&
                 $key !== 'updated_at' &&
                 $key !== 'id_preventivo' &&
                 $key !== 'comentario' &&
                 $key !== 'id_chequeo') {
                $chequeo->{$key} = $check[$value];
            }
        }
        return $chequeo;
    }
    public function getPreventivoData(){

        $Preventivo = Preventivo::with('cuenta','tecnico','estado_maquina','tipo_maquina','tipo_servicio','chequeo','estado_maquina')->get();
                        //->leftJoin('chequeo','chequeo.id_preventivo','preventivo.id_preventivo');
        $Preventivo = $Preventivo->map(function($item, $key){
            if ($item->chequeo)
            $item->chequeo = $this->editChequeo($item->chequeo);
            return $item;
        });
        return Datatables::of($Preventivo)
                ->editColumn('fecha', function($data){
                    return  $data->fecha ? $data->fecha->format('d/m/Y') : '';
                })
                ->editColumn('inicio_trabajo', function($data){
                    return $data->inicio_trabajo ? $data->inicio_trabajo->format('d/m/Y') : '';
                })
                ->editColumn('fin_trabajo', function($data){
                    return $data->fin_trabajo ? $data->fin_trabajo->format('d/m/Y') : '';
                })
                ->removeColumn('created_at')->removeColumn('updated_at')
                ->make(true);
    }

    public function getCorrectivoData()
    {
        $OrdenTrabajo = OrdenTrabajo::with('cuenta','tecnico','estado_maquina','tipo_maquina','tipo_servicio','aviso_segun','estado_maquina','origen_falla')->get();

        return Datatables::of($OrdenTrabajo)
                ->editColumn('fecha', function($data){
                    return $data->fecha->format('d/m/Y');
                })
                ->editColumn('inicio_trabajo', function($data){
                    return $data->inicio_trabajo->format('d/m/Y');
                })
                ->editColumn('fin_trabajo', function($data){
                    return $data->fin_trabajo->format('d/m/Y');
                })
                 ->editColumn('aplica_gasto', function($data){
                    return $data->aplica_gasto == 1 ? 'SI':'NO';
                })
                ->removeColumn('created_at')->removeColumn('updated_at')
                ->make(true);
    }

    public function getCuentaData(){
        $Cuenta = Cuenta::all();

        return Datatables::of($Cuenta)
                ->removeColumn('created_at')->removeColumn('updated_at')->removeColumn('deleted_at')
                ->addColumn('action', function($row) {
                    return '<button data-id="'.$row->id_cuenta.'" class="btn btn-danger btn-eliminar btn-sm"  >Desactivar</button>';
                })
                ->make();
    }

    public function getTecnicoData(){
        $Tecnico = User::where('rol',1)->get();

        return Datatables::of($Tecnico)
                ->addColumn('action', function($row) {
                    return '<button data-id="'.$row->id.'" class="btn btn-danger btn-eliminar btn-sm"  >Desactivar</button>';
                })
                ->removeColumn('created_at')
                ->removeColumn('updated_at')
                ->removeColumn('deleted_at')
                ->removeColumn('email_verified_at')
                ->removeColumn('password')
                ->removeColumn('rol')
                ->removeColumn('remember_token')
                ->make(true);
    }
  	public function getServicioData(){
        $TipoServicio = TipoServicio::all();

        return Datatables::of($TipoServicio)
                ->removeColumn('created_at')->removeColumn('updated_at')
                ->make();
    }
	public function getMaquinaData(){
        $TipoMaquina = TipoMaquina::all();

        return Datatables::of($TipoMaquina)
                ->removeColumn('created_at')->removeColumn('updated_at')
                ->make();
    }
    public function getPreventivo(){
        return view('datos.preventivo');
    }
    public function getCorrectivo(){
        return view('datos.correctivo');
    }
    public function getTecnico(){
        return view('datos.tecnico');
    }
    public function getCuenta(){
        return view('datos.cuenta');
    }
 	public function getServicio(){
        return view('datos.servicio');
    }
 	public function getMaquina(){
        return view('datos.maquina');
    }

    public function saveCuenta(Request $request){

        Cuenta::create(['tx_nombre'=>$request->cuenta]);

        return response()->json(true);

    }

    public function deleteCuenta(Request $request){

        Cuenta::find($request->id)->delete();

        return response()->json(true);
    }

    public function saveTecnico(Request $request){

        if ($request->password !== $request->conf_password) {
            return response()->json(['estado'=>false,'msj'=>'Password no coinciden.']);
        }

        $check = User::where('email',$request->email)->first();

        if($check){
            return response()->json(['estado'=>false,'msj'=>'Email ya existe.']);
        }

        $User = new User;
        $User->name = $request->nombre;
        $User->apellido = $request->apellido;
        $User->email = $request->email;
        $User->password = bcrypt($request->password);
        $User->rol = 1;
        $User->save();

        return response()->json(['estado'=>true]);
    }

    public function deleteTecnico(Request $request){

        User::find($request->id)->delete();

        return response()->json(true);
    }
}