<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoMaquina extends Model
{
   	protected $table = 'estado_maquina';
    protected $primaryKey = 'id_estado_maquina';
}
