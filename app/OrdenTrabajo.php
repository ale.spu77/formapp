<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\AvisoSegun;
use App\EstadoMaquina;
use App\TipoServicio;
use App\OrigenFalla;

class OrdenTrabajo extends Model
{
    
    protected $table = 'orden_trabajo';
    protected $primaryKey = 'id_orden';
    protected $dates = [
        'inicio_trabajo',
        'fin_trabajo',
        'fecha',
    ];

    public function aviso_segun(){
    	return $this->hasOne(AvisoSegun::class,'id_aviso_segun','id_aviso_segun');
    }
    public function estado_maquina(){
    	return $this->hasOne(EstadoMaquina::class,'id_estado_maquina','id_estado_maquina');
    }
    public function tipo_servicio(){
    	return $this->hasOne(TipoServicio::class,'id_tipo_servicio','id_tipo_servicio');
    }
    public function origen_falla(){
    	return $this->hasOne(OrigenFalla::class,'id_falla','id_origen_falla');
    }
    public function cuenta(){
        return $this->hasOne(Cuenta::class,'id_cuenta','id_cuenta');
    }
    public function tipo_maquina(){
        return $this->hasOne(TipoServicio::class,'id_tipo_servicio','id_tipo_servicio');
    }
    public function tecnico(){
        return $this->hasOne(Tecnico::class,'id_tecnico','id_tecnico');
    }
    
}
