<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Preventivo;

class Chequeo extends Model
{
    protected $table      = 'chequeo';
    protected $primaryKey = 'id_chequeo';
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function preventivo(){
    	return $this->hasOne(Preventivo::class,'id_preventivo','id_preventivo');
    }
  
}
