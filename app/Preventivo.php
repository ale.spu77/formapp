<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\AvisoSegun;
use App\EstadoMaquina;
use App\TipoServicio;
use App\OrigenFalla;
use App\Cuenta;
use App\Tecnico;

class Preventivo extends Model
{
    protected $table = 'preventivo';
    protected $primaryKey = 'id_preventivo';
    protected $dates = [
        'inicio_trabajo',
        'fin_trabajo',
        'fecha',
    ];

    public function aviso_segun(){
        return $this->hasOne(AvisoSegun::class,'id_aviso_segun','id_aviso_segun');
    }
    public function estado_maquina(){
    	return $this->hasOne(EstadoMaquina::class,'id_estado_maquina','id_estado_maquina');
    }
    public function tipo_servicio(){
    	return $this->hasOne(TipoServicio::class,'id_tipo_servicio','id_tipo_servicio');
    }
    public function tipo_maquina(){
        return $this->hasOne(TipoServicio::class,'id_tipo_servicio','id_tipo_servicio');
    }
    public function cuenta(){
        return $this->hasOne(Cuenta::class,'id_cuenta','id_cuenta');
    }
    public function chequeo(){
        return $this->hasOne(Chequeo::class,'id_preventivo','id_preventivo');
    }
    public function tecnico(){
        return $this->hasOne(Tecnico::class,'id_tecnico','id_tecnico');
    }
}
