<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AvisoSegun extends Model
{
    protected $table = 'aviso_segun';
    protected $primaryKey = 'id_aviso_segun';
}
