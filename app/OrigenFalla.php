<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrigenFalla extends Model
{
 	protected $table = 'origen_falla';
    protected $primaryKey = 'id_falla';
}
