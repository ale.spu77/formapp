<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/home', 'HomeController@panel_correctivo')->name('home');

Route::get('/', 'HomeController@panel_correctivo')->name('panel');
Route::get('/grafico/correctivo', 'HomeController@panel_correctivo')->name('graficos.correctivo');
Route::get('/grafico/preventivo', 'HomeController@panel_preventivo')->name('graficos.preventivo');
Route::get('/form/formulario', 'HomeController@getFormulario')->name('form.correctivo');
Route::get('/form/preventivo', 'HomeController@getPreventivo')->name('form.preventivo');
Route::post('/loadFiltro', 'HomeController@loadFiltro')->name('loadFiltro');
Route::post('/loadFiltroPreventivo', 'HomeController@loadFiltroPreventivo')->name('loadFiltroPreventivo');
Route::post('/saveForm', 'HomeController@saveForm')->name('saveForm');
Route::post('/saveFormPreventivo', 'HomeController@saveFormPreventivo')->name('saveFormPreventivo');

Route::get('/cuenta', 'DataController@getCuenta')->name('man.cuenta');
Route::get('/tecnico', 'DataController@getTecnico')->name('man.tecnico');
Route::get('/servicio', 'DataController@getServicio')->name('man.servicio');
Route::get('/maquina', 'DataController@getMaquina')->name('man.maquina');
Route::get('/correctivo', 'DataController@getCorrectivo')->name('man.correctivo');
Route::get('/preventivo', 'DataController@getPreventivo')->name('man.preventivo');

Route::get('/getCuentaData', 'DataController@getCuentaData')->name('getCuentaData');
Route::get('/getTecnicoData', 'DataController@getTecnicoData')->name('getTecnicoData');
Route::get('/getCorrectivoData', 'DataController@getCorrectivoData')->name('getCorrectivoData');
Route::get('/getPreventivoData', 'DataController@getPreventivoData')->name('getPreventivoData');
Route::get('/getServicioData', 'DataController@getServicioData')->name('getServicioData');

Route::post('/save/cuenta', 'DataController@saveCuenta')->name('save.cuenta');
Route::post('/delete/cuenta', 'DataController@deleteCuenta')->name('delete.cuenta');

Route::post('/save/tecnico', 'DataController@saveTecnico')->name('save.tecnico');
Route::post('/delete/tecnico', 'DataController@deleteTecnico')->name('delete.tecnico');
