@extends('layouts.app')

@section('script')

<script type="text/javascript">
(function($) {
  $.fn.inputFilter = function(inputFilter) {
    return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      }
    });
  };
}(jQuery));

	$(function() {

        $('.dp_fecha').datetimepicker({
		    format: 'L',
        	icons: {
		        time: 'fa fa-clock',
		        date: 'fa fa-calendar',
		        up: 'fa fa-chevron-up',
		        down: 'fa fa-chevron-down',
		        previous: 'fa fa-chevron-left',
		        next: 'fa fa-chevron-right',
		        today: 'fa fa-screenshot',
		        clear: 'fa fa-trash',
		        close: 'fa fa-remove'
		    }
        });
        $('.dp_mes').datetimepicker({
		    viewMode : 'months',
		    format : 'MM',
		    toolbarPlacement: "top",
		    //allowInputToggle: true,
		    icons: {
		        time: 'fa fa-time',
		        date: 'fa fa-calendar',
		        up: 'fa fa-chevron-up',
		        down: 'fa fa-chevron-down',
		        previous: 'fa fa-chevron-left',
		        next: 'fa fa-chevron-right',
		        today: 'fa fa-screenshot',
		        clear: 'fa fa-trash',
		        close: 'fa fa-remove'
		    }
		})

        $('.dp_hora').datetimepicker({format: 'HH:mm'});
	})

	$(".input_number").inputFilter(function(value) {
  		return /^\d*$/.test(value);
	});



	$('#form').submit(function(e){
		
		e.preventDefault();

		var formData = new FormData($('#form')[0]);
		
		$('#cssload').show();

		$.ajax({
			url:"{{route('saveFormPreventivo')}}",
			type:"POST",
			data:formData,
			processData: false,
  			contentType: false,
  			dataType: "json",
		}).done(function(data){

			$('#cssload').hide();
			alert('Guardado correctamente');
			$('#form').trigger("reset");

		});

		return false;
	});

</script>
@endsection

@section('content')
<style type="text/css">
	.form-group {
	    margin-bottom: 0.5rem;
	}
	label {
	    display: inline-block;
	    margin-bottom: .1rem;
	}

</style>
<div class="container-fluid ">
	<form id="form">

	    <div class="card" style="padding: 5px 10px">

	    	<h3 style="text-align: center;">PAUTAS DE MANTENIMIENTO PREVENTIVOS</h3>
	    		<div class="row" >
		    		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		    		<div class="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<label for="fehca">Fecha</label>
			            <input 
			            	type="text" 
			            	id="fecha" 
			            	name="fecha" 
			            	class="form-control form-control-sm datetimepicker-input dp_fecha" 
			            	placeholder="Fecha" 
			            	data-target="#fecha" data-toggle="datetimepicker" 
			            	required="">
					</div>
					<div class="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<label for="periodo">Mes Periodo</label>
			            <input type="text" 
			            	id="numero_periodo" 
			            	name="numero_periodo" 
			            	class="form-control form-control-sm dp_mes datetimepicker-input" 
			            	data-target="#numero_periodo" data-toggle="datetimepicker" 
			            	placeholder="Numero periodo" 
			            	required="required">
		    		</div>
					<div class="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<label for="numero_ot">Numero Pauta</label>
			            <input type="text" id="nu_pauta" name="nu_pauta" class="form-control form-control-sm input_number" placeholder="Numero Pauta" required="">
					</div>
					<div class="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<label for="cuenta">Cuenta</label>
			            <select  class="form-control form-control-sm" required="required" name="cuenta">
			            	<option value="0">--Seleccionar Cuenta--</option>
			            	@foreach($cuentas as $key => $cuenta)
			            		<option value="{{$key}}">{{ $cuenta }}</option>
			            	@endforeach
			            </select>
					</div>
					<div class="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<label for="tecnico">Tecnico</label>
			            <select class="form-control form-control-sm" required="required" name="tecnico">
			            	<option value="0">--Seleccionar Tecnico--</option>
			            	@foreach($tecnicos as $key => $tecnico)
			            		<option value="{{$key}}">{{ $tecnico }}</option>
			            	@endforeach
			            </select>
					</div>
					<div class="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<label for="estado_maquina">Estado Maquina</label>
			            <select class="form-control form-control-sm" required="required" name="estado_maquina">
			            	<option value="0">--Seleccionar Maquina--</option>
			            	@foreach($EstadoMaquina as $key => $estado)
			            		<option value="{{$key}}">{{ $estado }}</option>
			            	@endforeach
			            </select>
					</div>
					<div class="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<label for="inicio_trabajo">Inicio Trabajo</label>
			            <input type="text" 
			            	id="inicio_trabajo" 
			            	name="inicio_trabajo" 
			            	class="form-control form-control-sm dp_fecha datetimepicker-input" 
			            	placeholder="Inicio Trabajo"
			            	data-target="#inicio_trabajo" data-toggle="datetimepicker" 
			            	required="">
					</div>
					<div class="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<label for="hora_inicio">Hora Inicio</label>
			            <input 
			            	type="text" 
			            	id="hora_inicio" 
			            	name="hora_inicio" 
			            	class="form-control form-control-sm dp_hora datetimepicker-input"
			            	data-target="#hora_inicio" data-toggle="datetimepicker" 
			            	placeholder="Hora Inicio" 
			            	required="">
					</div>
					<div class="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<label for="fehca">Fin Trabajo</label>
			            <input 
			            	type="text" 
			            	id="fin_trabajo" 
			            	name="fin_trabajo" 
			            	class="form-control form-control-sm dp_fecha datetimepicker-input"
			            	data-target="#fin_trabajo" data-toggle="datetimepicker" 
			            	placeholder="Fin Trabajo" 
			            	required="">
					</div>
					<div class="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<label for="fehca">Hora Fin</label>
			            <input 
			            	type="text" 
			            	id="hora_fin" 
			            	name="hora_fin" 
			            	class="form-control form-control-sm dp_hora datetimepicker-input"
			            	data-target="#hora_fin" data-toggle="datetimepicker" 
			            	placeholder="Hora Fin" 
			            	required="">
					</div>
					<div class="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<label for="fehca">Dias Detencion</label>
			            <input 
			            	type="text" 
			            	id="dias_detencion" 
			            	name="dias_detencion" 
			            	class="form-control form-control-sm input_number" 
			            	placeholder="Dias Dentencion" 
			            	required="">
					</div>
					<div class="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<label for="fehca">Hora Detencion</label>
			            <input 
			            	type="text" 
			            	id="horas_detencion" 
			            	name="horas_detencion" 
			            	class="form-control form-control-sm input_number"
			            	placeholder="Hora Detencion" 
			            	required="">
					</div>
					
					<div class="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<label for="fehca">Numero Maquina</label>
			            <input 
			            	type="text" 
			            	id="numero_maquina" 
			            	name="numero_maquina" 
			            	class="form-control form-control-sm input_number" 
			            	placeholder="Numero Maquina" >
					</div>
					<div class="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<label for="fehca">Tipo Maquina</label>
			            <select class="form-control form-control-sm" required="required" name="tipo_maquina">
			            	<option value="0">--Seleccionar Tipo--</option>
			            	@foreach($tipoMaquina as $key => $maquina)
			            		<option value="{{$key}}">{{ $maquina }}</option>
			            	@endforeach
			            </select>
					</div>
					<div class="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<label for="fehca">Horometro</label>
			            <input 
			            	type="text" 
			            	id="horometro" 
			            	name="horometro" 
			            	class="form-control form-control-sm input_number" 
			            	placeholder="Horometro" >
					</div>
					<div class="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<label for="fehca">Tipo Servicio</label>
			           	<select class="form-control form-control-sm" required="required" name="tipo_servicio">
			            	<option value="0">--Seleccionar Tipo Servicio--</option>
			            	@foreach($TipoServicio as $key => $tipo)
			            		<option value="{{$key}}">{{ $tipo }}</option>
			            	@endforeach
			            </select>
					</div>
					<div class="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<label for="fehca">Cumplimiento</label>
			           	<select class="form-control form-control-sm" required="required" name="cumplimiento">
			            	<option value="100">100</option>
			            	<option value="0">0</option>
			            </select>
					</div>
					<div class="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<label for="observacion">Observacion</label>
						<textarea class="form-control" name="observacion"></textarea>
					</div>
					
				</div>

	    </div>

	    <div class="card card-body d-flex flex-column" style="margin-top: 15px; padding: 5px 10px">
	    	<h3 style="text-align: center;">LISTA DE CHEQUEO</h3>
			<div class="row" >

	    		<div class="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<label for="fehca">Inspeccion visual</label>
		           	<select class="form-control form-control-sm" required="required" name="inspeccion_visual">
		            	<option value="1">OK</option>
		            	<option value="2">Ajuste</option>
		            	<option value="3">Reparar</option>
		            	<option value="4">Urgente</option>
		            </select>
				</div>
				<div class="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<label for="fehca">Baterias y Cables</label>
		           	<select class="form-control form-control-sm" required="required" name="baterias_cables">
		            	<option value="1">OK</option>
		            	<option value="2">Ajuste</option>
		            	<option value="3">Reparar</option>
		            	<option value="4">Urgente</option>
		            </select>
				</div>
				<div class="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<label for="fehca">Limpieza y lubricacion</label>
		           	<select class="form-control form-control-sm" required="required" name="limpieza_lubricacion">
		            	<option value="1">OK</option>
		            	<option value="2">Ajuste</option>
		            	<option value="3">Reparar</option>
		            	<option value="4">Urgente</option>
		            </select>
				</div>
				<div class="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<label for="fehca">Bomba hidraulica y motor</label>
		           	<select class="form-control form-control-sm" required="required" name="bomba_hidraulica">
		            	<option value="1">OK</option>
		            	<option value="2">Ajuste</option>
		            	<option value="3">Reparar</option>
		            	<option value="4">Urgente</option>
		            </select>
				</div>
				<div class="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<label for="fehca">Unidad de traccion</label>
		           	<select class="form-control form-control-sm" required="required" name="unidad_traccion">
		            	<option value="1">OK</option>
		            	<option value="2">Ajuste</option>
		            	<option value="3">Reparar</option>
		            	<option value="4">Urgente</option>
		            </select>
				</div>
				<div class="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<label for="fehca">Cables de potencia y control</label>
		           	<select class="form-control form-control-sm" required="required" name="cables_potencia_control">
		            	<option value="1">OK</option>
		            	<option value="2">Ajuste</option>
		            	<option value="3">Reparar</option>
		            	<option value="4">Urgente</option>
		            </select>
				</div>
				<div class="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<label for="fehca">Panel contadores</label>
		           	<select class="form-control form-control-sm" required="required" name="panel_contadores">
		            	<option value="1">OK</option>
		            	<option value="2">Ajuste</option>
		            	<option value="3">Reparar</option>
		            	<option value="4">Urgente</option>
		            </select>
				</div>
				<div class="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<label for="fehca">Frenos</label>
		           	<select class="form-control form-control-sm" required="required" name="frenos">
		            	<option value="1">OK</option>
		            	<option value="2">Ajuste</option>
		            	<option value="3">Reparar</option>
		            	<option value="4">Urgente</option>
		            </select>
				</div>
				<div class="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<label for="fehca">Direccion</label>
		           	<select class="form-control form-control-sm" required="required" name="direccion">
		            	<option value="1">OK</option>
		            	<option value="2">Ajuste</option>
		            	<option value="3">Reparar</option>
		            	<option value="4">Urgente</option>
		            </select>
				</div>
				<div class="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<label for="fehca">Sistema Hidraulico</label>
		           	<select class="form-control form-control-sm" required="required" name="sistema_hidraulico">
		            	<option value="1">OK</option>
		            	<option value="2">Ajuste</option>
		            	<option value="3">Reparar</option>
		            	<option value="4">Urgente</option>
		            </select>
				</div>
				<div class="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<label for="fehca">Ensamble mastil</label>
		           	<select class="form-control form-control-sm" required="required" name="esamble_mastil">
		            	<option value="1">OK</option>
		            	<option value="2">Ajuste</option>
		            	<option value="3">Reparar</option>
		            	<option value="4">Urgente</option>
		            </select>
				</div>
				<div class="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<label for="fehca">Chequeo Operacional</label>
		           	<select class="form-control form-control-sm" required="required" name="chequeo_operacional">
		            	<option value="1">OK</option>
		            	<option value="2">Ajuste</option>
		            	<option value="3">Reparar</option>
		            	<option value="4">Urgente</option>
		            </select>
				</div>
				<div class="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<label for="fehca">Aditamentos</label>
		           	<select class="form-control form-control-sm" required="required" name="aditamientos">
		            	<option value="1">OK</option>
		            	<option value="2">Ajuste</option>
		            	<option value="3">Reparar</option>
		            	<option value="4">Urgente</option>
		            </select>
				</div>
				<div class="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<label for="fehca">Cerrar equipo y limpieza</label>
		           	<select class="form-control form-control-sm" required="required" name="equipo_limpieza">
		            	<option value="1">OK</option>
		            	<option value="2">Ajuste</option>
		            	<option value="3">Reparar</option>
		            	<option value="4">Urgente</option>
		            </select>
				</div>
				<div class="form-group col-xs-12 col-sm-12 col-md-7 col-lg-7">
					<label for="fehca">COMENTARIOS</label>
					<textarea class="form-control" name="comentario"></textarea>
				</div>
				<div style="padding: 0px" class="card-body d-flex flex-column col-xs-12 col-sm-12 col-md-3 col-lg-3">
					<button type="submit" class="btn btn-primary align-self-end">Guardar</button>
				</div>
			</div>
	    </div>
	</form>	
</div>



<div id="cssload" class="cssload-container">
	<div class="cssload-whirlpool"></div>
</div>

<style type="text/css">

.cssload-container{
	position:fixed;
	top: 50%;
    left: 50%;
    z-index: 1000;
    display: none;
}
	
.cssload-whirlpool,
.cssload-whirlpool::before,
.cssload-whirlpool::after {
	position: absolute;
	top: 50%;
	left: 50%;
	border: 1px solid rgb(157,157,163);
	border-left-color: rgb(0,0,0);
	border-radius: 974px;
		-o-border-radius: 974px;
		-ms-border-radius: 974px;
		-webkit-border-radius: 974px;
		-moz-border-radius: 974px;
}

.cssload-whirlpool {
	margin: -24px 0 0 -24px;
	height: 49px;
	width: 49px;
	animation: cssload-rotate 1150ms linear infinite;
		-o-animation: cssload-rotate 1150ms linear infinite;
		-ms-animation: cssload-rotate 1150ms linear infinite;
		-webkit-animation: cssload-rotate 1150ms linear infinite;
		-moz-animation: cssload-rotate 1150ms linear infinite;
}

.cssload-whirlpool::before {
	content: "";
	margin: -22px 0 0 -22px;
	height: 43px;
	width: 43px;
	animation: cssload-rotate 1150ms linear infinite;
		-o-animation: cssload-rotate 1150ms linear infinite;
		-ms-animation: cssload-rotate 1150ms linear infinite;
		-webkit-animation: cssload-rotate 1150ms linear infinite;
		-moz-animation: cssload-rotate 1150ms linear infinite;
}

.cssload-whirlpool::after {
	content: "";
	margin: -28px 0 0 -28px;
	height: 55px;
	width: 55px;
	animation: cssload-rotate 2300ms linear infinite;
		-o-animation: cssload-rotate 2300ms linear infinite;
		-ms-animation: cssload-rotate 2300ms linear infinite;
		-webkit-animation: cssload-rotate 2300ms linear infinite;
		-moz-animation: cssload-rotate 2300ms linear infinite;
}



@keyframes cssload-rotate {
	100% {
		transform: rotate(360deg);
	}
}

@-o-keyframes cssload-rotate {
	100% {
		-o-transform: rotate(360deg);
	}
}

@-ms-keyframes cssload-rotate {
	100% {
		-ms-transform: rotate(360deg);
	}
}

@-webkit-keyframes cssload-rotate {
	100% {
		-webkit-transform: rotate(360deg);
	}
}

@-moz-keyframes cssload-rotate {
	100% {
		-moz-transform: rotate(360deg);
	}
}
</style>
@endsection