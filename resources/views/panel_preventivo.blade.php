@extends('layouts.app')

@section('script')
<script type="text/javascript">

    var barChartMantenimiento = $("#barChartMantenimiento").get(0).getContext("2d");
    var barChartTurno = $("#barChartTurno").get(0).getContext("2d");

    $(document).ready(function(){

        barChartMantenimiento = new Chart(barChartMantenimiento, {
            type: 'doughnut',
            showTooltips: true,
            data: {
                labels: [],
                datasets: [{
                    data: [],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.5)',
                        'rgba(54, 162, 235, 0.5)',
                        'rgba(255, 206, 86, 0.5)',
                        'rgba(75, 192, 192, 0.5)',
                        'rgba(153, 102, 255, 0.5)',
                        'rgba(255, 159, 64, 0.5)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                        ],
                    }]
            },
            options: {
                responsive: true,
                legend: {
                  display: true
                },
                tooltips: {
                    enabled: true
                },
                plugins: {
                    datalabels: {
                        formatter: (value, ctx) => {
                            let sum = 0;
                            let dataArr = ctx.chart.data.datasets[0].data;
                            dataArr.map(data => {
                                sum += data;
                            });
                            let percentage = (value*100 / sum).toFixed(2)+"%";
                            return percentage;
                        },
                        color: '#000',
                    }
                },
                animation: {
                    animateScale: true,
                    animateRotate: true,
                }
            }
        });
        
        barChartTurno = new Chart(barChartTurno, {
            type: 'doughnut',
            showTooltips: true,
            data: {
                labels: [],
                datasets: [{
                data: [],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.5)',
                    'rgba(54, 162, 235, 0.5)',
                    'rgba(255, 206, 86, 0.5)',
                    'rgba(75, 192, 192, 0.5)',
                    'rgba(153, 102, 255, 0.5)',
                    'rgba(255, 159, 64, 0.5)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                    ],
                }]
            },
            options: {
                responsive: true,
                legend: {
                  display: true
                },
                tooltips: {
                    enabled: true
                },
                plugins: {
                    datalabels: {
                        formatter: (value, ctx) => {
                            let sum = 0;
                            let dataArr = ctx.chart.data.datasets[0].data;
                            dataArr.map(data => {
                                sum += data;
                            });
                            let percentage = (value*100 / sum).toFixed(2)+"%";
                            return percentage;
                        },
                        color: '#000',
                    }
                },
                animation: {
                    animateScale: true,
                    animateRotate: true,
                }
            }
        });

        loadFiltro();

    });

    $('#fil_ano,#fil_estado_maquina,#fil_origen_falla,#fil_periodo,#fil_cuenta,#fil_tipo_maquina').on('change',function(){
        console.log('asdasd');
        loadFiltro();
    });

    function loadFiltro(){

        var ano            = $('#fil_ano').val();
        var periodo        = $('#fil_periodo').val();
        var cuenta         = $('#fil_cuenta').val();
        removeData(barChartMantenimiento);
        removeData(barChartTurno);
        $.ajax({
            url:"{{url('loadFiltroPreventivo')}}",
            type:"POST",
            data:{
                ano:ano,
                periodo:periodo,
                cuenta:cuenta,
                _token:"{{csrf_token()}}"
            }
        }).done(function(data){
            
            $('#td_horas_utilizadas').html(data.HorasUtilizacion);
            $('#td_cumplimiento').html(data.DisponibilidadTotal);

            $.each(data.Mantenimiento,function(key,item){
               addData(barChartMantenimiento,key,item);
            });
            $.each(data.Turno,function(key,item){
               addData(barChartTurno,key,item);
            });
            //console.log(Object.keys(data.AccionMantenimiento));
        });
    }

    
     function addSetData(chart, label, data){
        chart.data.labels.push(label);
        chart.data.datasets.forEach((dataset) => {
            dataset.data.push(data);
        });
        chart.update();
    }
    function addData(chart, label, data){
        chart.data.labels.push(label);
        chart.data.datasets.forEach((dataset) => {
            dataset.data.push(data);
        });
        chart.update();
    }

    function removeData(chart) {
        chart.data.labels = [];
        chart.data.datasets[0].data = [];
        chart.update();
    }
</script>
@endsection


@section('content')
<style type="text/css">
.content-wrapper {
    /*background: #89b0ec !important;*/
}
#tabla-info tr th,#tabla-info tr td{
    padding: 5px !important;
    font-size: 10px;
    color: #fff
}
#tabla-info tr th {
    border-bottom-color: red;
}
.nav-item.nav-profile.dropdown a.nav-link span{
    color: #fff !important;
}
.nav-item.nav-profile.dropdown a.nav-link::after{
    color: #fff !important;
}
.stretch-card{
    padding: 5px
}
label{
    color: #fff
}
</style>

<div class="container-fluid">
    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        
        <div class="row">
            <h4 style="color: #fff">Preventivo</h4>
        </div>
        <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2">
                    <div class="form-group">
                        <label class="title-input">Año</label>
                        <select id="fil_ano" class="form-control form-control-sm"  multiple="multiple">
                            <option value="2019" selected>2019</option>
                            <option value="2018">2018</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2">
                    <div class="form-group">
                        <label class="title-input">Periodo</label>
                        <select id="fil_periodo" class="form-control form-control-sm"  multiple="multiple">
                            <option selected value="1" >1</option>
                            <option selected value="2" >2</option>
                            <option selected value="3" >3</option>
                            <option selected value="4" >4</option>
                            <option selected value="5" >5</option>
                            <option selected value="6" >6</option>
                            <option selected value="7" >7</option>
                            <option selected value="8" >8</option>
                            <option selected value="9" >9</option>
                            <option selected value="10" >10</option>
                            <option selected value="11" >11</option>
                            <option selected value="12" >12</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2">
                    <div class="form-group">
                        <label class="title-input">Cuentas</label>
                        <select id="fil_cuenta" class="form-control form-control-sm"  multiple="multiple">
                            @foreach($cuentas as $cuenta)
                            <option value="{{$cuenta->id_cuenta}}" selected>{{$cuenta->tx_nombre}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
        </div>
        <div class="row">
            <table id="tabla-info" border="0" class="table">
                <thead>
                    <tr>
                        <th>Horas Utilizadas</th>
                        <th>Porcentaje Cumplimiento:</th>
                    <tr>
                </thead>
                <tbody>
                    <tr>
                        <td id="td_horas_utilizadas" >0</td>
                        <td id="td_cumplimiento" >0</td>
                    <tr>
                </tbody>
            </table>    
        </div>
        <div class="row" style="margin-top: 20px">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6  stretch-card">
                <div class="card">
                    <div class="card-body" style="padding: 5px">
                        <canvas id="barChartMantenimiento"></canvas>
                    </div>
                </div>
                <div class="card" style="margin-top: 10px">
                    <div class="card-body" style="padding: 5px">
                        <canvas id="barChartTurno"></canvas>
                    </div>
                </div>
            </div>     
        </div>
    </div>
</div>

@endsection