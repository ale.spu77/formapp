@extends('layouts.app')

@section('script')
<script type="text/javascript">

$(document).ready( function () {
    $('#table_correctivo').DataTable({
    	responsive: true,
    	processing: true,
        serverSide: true,
        ajax: '{{ route("getPreventivoData") }}',
        columns: [
            {data: 'id_preventivo', name: 'id_preventivo'},
            {data: 'fecha',name:'fecha'},
			{data: 'nu_periodo',name:'nu_periodo'},
			{data: 'nu_pauta',name:'nu_pauta'},
			{data: 'cuenta.tx_nombre',name:'cuenta.id_cuenta'},
			{data: 'tecnico.tx_nombre',name:'id_tecnico'},
			{data: 'estado_maquina.tx_nombre',name:'id_estado_maquina'},
			{data: 'inicio_trabajo',name:'inicio_trabajo'},
			{data: 'hora_inicio',name:'hora_inicio'},
			{data: 'fin_trabajo',name:'fin_trabajo'},
			{data: 'hora_fin',name:'hora_fin'},
			{data: 'dias_detencion',name:'dias_detencion'},
			{data: 'horas_detencion',name:'horas_detencion'},
			{data: 'numero_maquina',name:'numero_maquina'},
			{data: 'tipo_maquina.tx_nombre',name:'id_tipo_maquina'},
			{data: 'horometro',name:'horometro'},
			{data: 'tipo_servicio.tx_nombre',name:'id_tipo_servicio'},
			{data: 'descripcion',name:'descripcion'},
			{data: 'chequeo.inspeccion_visual',name:'inspeccion_visual'},
			{data: 'chequeo.baterias_cables',name:'baterias_cables'},
			{data: 'chequeo.limpieza_lubricacion',name:'limpieza_lubricacion'},
			{data: 'chequeo.bomba_hidraulica',name:'bomba_hidraulica'},
			{data: 'chequeo.unidad_traccion',name:'unidad_traccion'},
			{data: 'chequeo.cables_potencia_control',name:'cables_potencia_control'},
			{data: 'chequeo.panel_contadores',name:'panel_contadores'},
			{data: 'chequeo.frenos',name:'frenos'},
			{data: 'chequeo.direccion',name:'direccion'},
			{data: 'chequeo.sistema_hidraulico',name:'sistema_hidraulico'},
			{data: 'chequeo.esamble_mastil',name:'esamble_mastil'},
			{data: 'chequeo.chequeo_operacional',name:'chequeo_operacional'},
			{data: 'chequeo.aditamientos',name:'aditamientos'},
			{data: 'chequeo.equipo_limpieza',name:'equipo_limpieza'},
        ],
        language: {
			  "sProcessing":     "Procesando...",
			  "sLengthMenu":     "Mostrar _MENU_ registros",
			  "sZeroRecords":    "No se encontraron resultados",
			  "sEmptyTable":     "Ningún dato disponible en esta tabla",
			  "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			  "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
			  "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			  "sInfoPostFix":    "",
			  "sSearch":         "Buscar:",
			  "sUrl":            "",
			  "sInfoThousands":  ",",
			  "sLoadingRecords": "Cargando...",
			  "oPaginate": {
			    "sFirst":    "Primero",
			    "sLast":     "Último",
			    "sNext":     "Siguiente",
			    "sPrevious": "Anterior"
			  },
			  "oAria": {
			    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
			    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
			  }
	  	}
    });
} );

</script>
@endsection

@section('content')
	


<div class="container-fluid">
	<div class="card" style="padding: 10px">

		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="table-responsive">
					<table id="table_correctivo" class="table table-sm table-hover">
						<thead>
							<tr>
								<th>id_preventivo</th>
								<th>nu_periodo</th>
								<th>fecha</th>
								<th>nu_pauta</th>
								<th>id_cuenta</th>
								<th>id_tecnico</th>
								<th>id_estado_maquina</th>
								<th>inicio_trabajo</th>
								<th>hora_inicio</th>
								<th>fin_trabajo</th>
								<th>hora_fin</th>
								<th>dias_detencion</th>
								<th>horas_detencion</th>								
								<th>numero_maquina</th>
								<th>id_tipo_maquina</th>
								<th>horometro</th>																
								<th>id_tipo_servicio</th>								
								<th>descripcion</th>
								<th>inspeccion_visual</th>
								<th>baterias_cables</th>
								<th>limpieza_lubricacion</th>
								<th>bomba_hidraulica</th>
								<th>unidad_traccion</th>
								<th>cables_potencia_control</th>
								<th>panel_contadores</th>
								<th>frenos</th>
								<th>direccion</th>
								<th>sistema_hidraulico</th>
								<th>esamble_mastil</th>
								<th>chequeo_operacional</th>
								<th>aditamientos</th>
								<th>equipo_limpieza</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>

	</div>
</div>




@endsection
