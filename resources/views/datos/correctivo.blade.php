@extends('layouts.app')

@section('script')
<script type="text/javascript">

$(document).ready( function () {
    $('#table_correctivo').DataTable({
    	responsive: true,
    	processing: true,
        serverSide: true,
        ajax: '{{ route("getCorrectivoData") }}',
        columns: [
	        {data: 'id_orden', name: 'id_preventivo'},
	        {data: 'nu_periodo', name: 'nu_periodo'},
	        {data: 'fecha',name:'fecha'},
			{data: 'nu_iso_tools',name:'nu_iso_tools'},
			{data: 'nu_ot',name:'nu_ot'},
			{data: 'cuenta.tx_nombre',name:'cuenta.id_cuenta'},
			{data: 'tecnico.tx_nombre',name:'id_tecnico'},
			{data: 'estado_maquina.tx_nombre',name:'id_estado_maquina'},
			{data: 'inicio_trabajo',name:'inicio_trabajo'},
			{data: 'hora_inicio',name:'hora_inicio'},
			{data: 'fin_trabajo',name:'fin_trabajo'},
			{data: 'hora_fin',name:'hora_fin'},
			{data: 'dias_detencion',name:'dias_detencion'},
			{data: 'horas_detencion',name:'horas_detencion'},
			{data: 'aplica_gasto',name:'aplica_gasto'},
			{data: 'numero_maquina',name:'numero_maquina'},
			{data: 'tipo_maquina.tx_nombre',name:'id_tipo_maquina'},
			{data: 'horometro',name:'horometro'},
			{data: 'origen_falla.tx_nombre',name:'origen_falla'},
			{data: 'aviso_segun.tx_nombre',name:'aviso_segun'},
			{data: 'tipo_servicio.tx_nombre',name:'tipo_servicio'},
			{data: 'nu_presupuesto',name:'nu_presupuesto'},
			{data: 'monto_reparacion',name:'monto_reparacion'},
			{data: 'monto_arriendo',name:'monto_arriendo'},
			{data: 'descripcion',name:'descripcion'},
		],
        language: {
			  "sProcessing":     "Procesando...",
			  "sLengthMenu":     "Mostrar _MENU_ registros",
			  "sZeroRecords":    "No se encontraron resultados",
			  "sEmptyTable":     "Ningún dato disponible en esta tabla",
			  "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			  "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
			  "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			  "sInfoPostFix":    "",
			  "sSearch":         "Buscar:",
			  "sUrl":            "",
			  "sInfoThousands":  ",",
			  "sLoadingRecords": "Cargando...",
			  "oPaginate": {
			    "sFirst":    "Primero",
			    "sLast":     "Último",
			    "sNext":     "Siguiente",
			    "sPrevious": "Anterior"
			  },
			  "oAria": {
			    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
			    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
			  }
	  	}
    });
} );

</script>
@endsection

@section('content')
	


<div class="container-fluid">
	<div class="card" style="padding: 10px">

		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="table-responsive">
					<table id="table_correctivo" class="table table-sm table-hover">
						<thead>
							<tr>
								<th>ID Correctivo</th>
								<th>Periodo</th>
								<th>Fecha</th>
								<th>Iso Tools</th>
								<th>Numero OT</th>
								<th>Cuenta</th>
								<th>Tecnico</th>
								<th>Estado Maquina</th>
								<th>Inicio Trabajo</th>
								<th>Hora Inicio</th>
								<th>Fin Trabajo</th>
								<th>Hora Fin</th>
								<th>Dias Detencion</th>
								<th>Horas Detencion</th>
								<th>Aplica Gasto</th>
								<th>Numero Maquina</th>
								<th>Tipo Maquina</th>
								<th>Horometro</th>
								<th>Origen Falla</th>
								<th>Aviso Segun</th>
								<th>Tipo Servicio</th>
								<th>Presupuesto</th>
								<th>Monto Reparacion</th>
								<th>Monto Arriendo</th>
								<th>Descripcion</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>

	</div>
</div>




@endsection
