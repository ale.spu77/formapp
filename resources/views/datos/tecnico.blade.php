@extends('layouts.app')

@section('script')
<script type="text/javascript">

$(document).ready( function () {

	$('#save-tecnico').on('click',function(){

		var nombre   = $('#nombre').val();
		var apellido = $('#apellido').val();
		var email    = $('#email').val();
		var password = $('#password').val();
		var conf_password = $('#conf_password').val();

		if (nombre == ''){
			alert('Falta nombre tecnico.');
			return false;
		}
		if (apellido == ''){
			alert('Falta apellido tecnico.');
			return false;
		}
		if (email == ''){
			alert('Falta email tecnico.');
			return false;
		}
		if (password == ''){
			alert('Falta password tecnico.');
			return false;
		}
		if (conf_password == ''){
			alert('Falta confirmar password tecnico.');
			return false;
		}

		$.ajax({
			type:"POST",
			url:"{{ route('save.tecnico') }}",
			data:{
				nombre:nombre,
				apellido:apellido,
				email:email,
				password:password,
				conf_password:conf_password,
				_token:"{{csrf_token()}}"
			}
		}).done(function(data){
			if (data.estado){
				alert('Tecnico guardado.');
				$('#modal-tecnico').modal('hide');
				dt.ajax.reload();
			}else{
				alert(data.msj)
			}
		});

	});

	dt = $('#table_tecnico').on('click','.btn-eliminar',function(){

		var id = $(this).data('id');

		$.ajax({
			type:"POST",
			url:"{{ route('delete.tecnico') }}",
			data:{
				id:id,
				_token:"{{csrf_token()}}"
			}
		}).done(function(){
			alert('Tecnico desactivado.');
			dt.ajax.reload();
		});

	});


    dt = $('#table_tecnico').DataTable({
    	columns: [
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            { data: 'apellido', name: 'apellido' },
            { data: 'email', name: 'email' },
            { data: 'action', name: 'action' }
        ],
    	responsive: true,
    	processing: true,
        serverSide: true,
        ajax: '{{ route("getTecnicoData") }}',
        language: {
			  "sProcessing":     "Procesando...",
			  "sLengthMenu":     "Mostrar _MENU_ registros",
			  "sZeroRecords":    "No se encontraron resultados",
			  "sEmptyTable":     "Ningún dato disponible en esta tabla",
			  "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			  "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
			  "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			  "sInfoPostFix":    "",
			  "sSearch":         "Buscar:",
			  "sUrl":            "",
			  "sInfoThousands":  ",",
			  "sLoadingRecords": "Cargando...",
			  "oPaginate": {
			    "sFirst":    "Primero",
			    "sLast":     "Último",
			    "sNext":     "Siguiente",
			    "sPrevious": "Anterior"
			  },
			  "oAria": {
			    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
			    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
			  }
	  	}
    });
} );

</script>
@endsection

@section('content')
	
<div class="container-fluid">

	<button type="button" data-toggle="modal" href='#modal-tecnico' class="btn btn-info" style="margin-bottom: 10px">Nuevo Tecnico</button>

	<div class="card" style="padding: 10px">

		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="table-responsive">
					<table id="table_tecnico" class="table table-sm table-hover">
						<thead>
							<tr>
								<th>ID Tecnico</th>
								<th>Nombre Tecnico</th>
								<th>Apellidos</th>
								<th>Email</th>
								<th>Accion</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>

	</div>
</div>

<div class="modal fade" id="modal-tecnico">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Nuevo Tecnico</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">

                <div class="form-group row">
                    <label for="nombre" class="col-md-4 col-form-label text-md-right">Nombres</label>

                    <div class="col-md-6">
                        <input id="nombre" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="apellido" class="col-md-4 col-form-label text-md-right">Apellidos</label>

                    <div class="col-md-6">
                        <input id="apellido" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                    </div>
                </div>


                <div class="form-group row">
                    <label for="email" class="col-md-4 col-form-label text-md-right">Email</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="conf_password" class="col-md-4 col-form-label text-md-right">Confirmar Password</label>

                    <div class="col-md-6">
                        <input id="conf_password" type="password" class="form-control" name="password_confirmation" required>
                    </div>
                </div>
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="button" class="btn btn-primary" id="save-tecnico">Guardar</button>
			</div>
		</div>
	</div>
</div>


@endsection
