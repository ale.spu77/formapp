@extends('layouts.app')

@section('script')
<script type="text/javascript">

$(document).ready( function () {
    $('#table_correctivo').DataTable({
    	responsive: true,
    	processing: true,
        serverSide: true,
        ajax: '{{ route("getServicioData") }}',
        language: {
			  "sProcessing":     "Procesando...",
			  "sLengthMenu":     "Mostrar _MENU_ registros",
			  "sZeroRecords":    "No se encontraron resultados",
			  "sEmptyTable":     "Ningún dato disponible en esta tabla",
			  "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			  "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
			  "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			  "sInfoPostFix":    "",
			  "sSearch":         "Buscar:",
			  "sUrl":            "",
			  "sInfoThousands":  ",",
			  "sLoadingRecords": "Cargando...",
			  "oPaginate": {
			    "sFirst":    "Primero",
			    "sLast":     "Último",
			    "sNext":     "Siguiente",
			    "sPrevious": "Anterior"
			  },
			  "oAria": {
			    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
			    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
			  }
	  	}
    });
} );

</script>
@endsection

@section('content')
	
<div class="container-fluid">
	<div class="card" style="padding: 10px">

		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="table-responsive">
					<table id="table_correctivo" class="table table-sm table-hover">
						<thead>
							<tr>
								<th>ID Servicio</th>
								<th>Nombre Servicio</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>

	</div>
</div>




@endsection
