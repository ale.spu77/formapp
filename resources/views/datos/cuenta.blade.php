@extends('layouts.app')

@section('script')
<script type="text/javascript">

$(document).ready( function () {

	$('#save-cuenta').on('click',function(){

		var cuenta = $('#cuenta').val();
		if (cuenta == ''){
			alert('Falta nombre de la cuenta.');
			return false;
		}
		$.ajax({
			type:"POST",
			url:"{{ route('save.cuenta') }}",
			data:{
				cuenta:cuenta,
				_token:"{{csrf_token()}}"
			}
		}).done(function(){

			alert('Cuenta guardada.');
			$('#modal-cuenta').modal('hide');
			dt.ajax.reload();
		});

	});

	$('#table_cuenta').on('click','.btn-eliminar',function(){

		var id = $(this).data('id');

		$.ajax({
			type:"POST",
			url:"{{ route('delete.cuenta') }}",
			data:{
				id:id,
				_token:"{{csrf_token()}}"
			}
		}).done(function(){

			alert('Cuenta desactivada.');
			dt.ajax.reload();
		});

	});

    dt = $('#table_cuenta').DataTable({
    	responsive: true,
    	processing: true,
        serverSide: true,
        ajax: '{{ route("getCuentaData") }}',
        language: {
			  "sProcessing":     "Procesando...",
			  "sLengthMenu":     "Mostrar _MENU_ registros",
			  "sZeroRecords":    "No se encontraron resultados",
			  "sEmptyTable":     "Ningún dato disponible en esta tabla",
			  "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			  "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
			  "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			  "sInfoPostFix":    "",
			  "sSearch":         "Buscar:",
			  "sUrl":            "",
			  "sInfoThousands":  ",",
			  "sLoadingRecords": "Cargando...",
			  "oPaginate": {
			    "sFirst":    "Primero",
			    "sLast":     "Último",
			    "sNext":     "Siguiente",
			    "sPrevious": "Anterior"
			  },
			  "oAria": {
			    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
			    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
			  }
	  	}
    });
} );

</script>
@endsection

@section('content')
	
<style type="text/css">
	
	td {
		padding: 2px !important
	}

</style>

<div class="container-fluid">
	<button type="button" data-toggle="modal" href='#modal-cuenta' class="btn btn-info" style="margin-bottom: 10px">Nueva Cuenta</button>

	<div class="card" style="padding: 10px">

		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="table-responsive">
					<table id="table_cuenta" class="table table-sm table-hover">
						<thead>
							<tr>
								<th>ID Cuenta</th>
								<th>Nombre Cuenta</th>
								<th>Accion</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>

	</div>
</div>


<div class="modal fade" id="modal-cuenta">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Nueva Cuenta</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				
				<div class="form-group">
					<label for="">Nombre</label>
					<input type="text" class="form-control" id="cuenta" >
				</div>
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="button" class="btn btn-primary" id="save-cuenta">Guardar</button>
			</div>
		</div>
	</div>
</div>


@endsection
