<!-- partial:partials/_sidebar.html -->
<nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link" href="{{url('panel')}}">
              <i class="mdi mdi-home menu-icon"></i>
              <span class="menu-title">Inicio</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
              <i class="mdi mdi-bulletin-board menu-icon"></i>
              <span class="menu-title">Datos</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-basic">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="{{url('clientes')}}">Clientes2</a></li>
                <li class="nav-item"> <a class="nav-link" href="{{url('clientes')}}">Cuentas</a></li>
                <li class="nav-item"> <a class="nav-link" href="{{url('formulario')}}">Formulario</a></li>
              </ul>
            </div>
          </li>
         
        </ul>
</nav>