@extends('layouts.app')

@section('script')
<script type="text/javascript">
    var barChartAviso = $("#barChartAviso").get(0).getContext("2d");
    var barChartCostoMantenimiento = $("#barChartCostoMantenimiento").get(0).getContext("2d");
    var barChartAlcance = $("#barChartAlcance").get(0).getContext("2d");
    var barChartOrigenFalla = $("#barChartOrigenFalla").get(0).getContext("2d");

    $(document).ready(function(){

        // This will get the first returned node in the jQuery collection.
        barChartAviso = new Chart(barChartAviso, {
            type: 'bar',
            showTooltips: false,
            data: {
                labels: [],
                datasets: [{
                    label:['Acciones de Mantenimiento'],
                    data: [],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 2,
                    fill: true
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
                animation: {
                    duration: 0
                },
                responsive: true,
                legend: {
                    display: true
                },
                tooltips: {
                    enabled: true
                },
                plugins: {
                    datalabels: {
                        formatter: (value, ctx) => {
                            let sum = 0;
                            let dataArr = ctx.chart.data.datasets[0].data;
                            dataArr.map(data => {
                                sum += data;
                            });
                            let percentage = (value*100 / sum).toFixed(2)+"%";
                            return value;
                        },
                        color: '#000',
                    }
                },
                tooltips: {
                    callbacks: {
                        title: function(chart, data) {
                          return data.labels[chart[0].datasetIndex];
                        }
                    }
                }
            }
        });

        barChartCostoMantenimiento = new Chart(barChartCostoMantenimiento, {
            type: 'bar',
            data: {
                labels: [],
                datasets: [{
                  label: 'Costo de Mantenimiento',
                  data: [],
                  backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                  ],
                  borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                  ],
                  borderWidth: 2,
                  fill: false
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
                responsive: true,
                legend: {
                  display: true
                },
                tooltips: {
                    enabled: true
                },
                plugins: {
                    datalabels: {
                        formatter: (value, ctx) => {
                            let sum = 0;
                            let dataArr = ctx.chart.data.datasets[0].data;
                            dataArr.map(data => {
                                sum += data;
                            });
                            //let percentage = (value*100 / sum).toFixed(2)+"%";
                            return value;
                        },
                        color: '#000',
                        anchor:'end',
                        align:'end',
                    }
                },
            }
        });

        barChartAlcance = new Chart(barChartAlcance, {
            type: 'doughnut',
            showTooltips: true,
            data: {
                labels: [],
                datasets: [{
                    data: [],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.5)',
                        'rgba(54, 162, 235, 0.5)',
                        'rgba(255, 206, 86, 0.5)',
                        'rgba(75, 192, 192, 0.5)',
                        'rgba(153, 102, 255, 0.5)',
                        'rgba(255, 159, 64, 0.5)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                        ],
                    }]
            },
            options: {
                responsive: true,
                legend: {
                  display: true
                },
                tooltips: {
                    enabled: true
                },
                plugins: {
                    datalabels: {
                        formatter: (value, ctx) => {
                            let sum = 0;
                            let dataArr = ctx.chart.data.datasets[0].data;
                            dataArr.map(data => {
                                sum += data;
                            });
                            let percentage = (value*100 / sum).toFixed(2)+"%";
                            return percentage;
                        },
                        color: '#000',
                    }
                },
                animation: {
                    animateScale: true,
                    animateRotate: true,
                }
            }
        });
        
        barChartOrigenFalla = new Chart(barChartOrigenFalla, {
            type: 'doughnut',
            showTooltips: true,
            data: {
                labels: [],
                datasets: [{
                data: [],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.5)',
                    'rgba(54, 162, 235, 0.5)',
                    'rgba(255, 206, 86, 0.5)',
                    'rgba(75, 192, 192, 0.5)',
                    'rgba(153, 102, 255, 0.5)',
                    'rgba(255, 159, 64, 0.5)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                    ],
                }]
            },
            options: {
                responsive: true,
                legend: {
                  display: true
                },
                tooltips: {
                    enabled: true
                },
                plugins: {
                    datalabels: {
                        formatter: (value, ctx) => {
                            let sum = 0;
                            let dataArr = ctx.chart.data.datasets[0].data;
                            dataArr.map(data => {
                                sum += data;
                            });
                            let percentage = (value*100 / sum).toFixed(2)+"%";
                            return percentage;
                        },
                        color: '#000',
                    }
                },
                animation: {
                    animateScale: true,
                    animateRotate: true,
                }
            }
        });

        loadFiltro();

    });

    $('#fil_ano,#fil_estado_maquina,#fil_origen_falla,#fil_periodo,#fil_cuenta,#fil_tipo_maquina').on('change',function(){
        console.log('asdasd');
        loadFiltro();
    });

    function loadFiltro(){

        var ano            = $('#fil_ano').val();
        var estado_maquina = $('#fil_estado_maquina').val();
        var origen_falla   = $('#fil_origen_falla').val();
        var periodo        = $('#fil_periodo').val();
        var cuenta         = $('#fil_cuenta').val();
        var tipo_maquina   = $('#fil_tipo_maquina').val();
        removeData(barChartAviso);
        removeData(barChartCostoMantenimiento);
        removeData(barChartAlcance);
        removeData(barChartOrigenFalla);
        $.ajax({
            url:"{{url('loadFiltro')}}",
            type:"POST",
            data:{
                ano:ano,
                estado_maquina:estado_maquina,
                origen_falla:origen_falla,
                periodo:periodo,
                cuenta:cuenta,
                tipo_maquina:tipo_maquina,
                _token:"{{csrf_token()}}"
            }
        }).done(function(data){
            
            $('#td_hora_detencion').html(data.HorasDetencion);
            $('#td_totales').html(data.HorasTotales);
            $('#td_monto_reparacion').html(data.MontoReparacion);
            $('#td_arriendo').html(data.MontoArriendo);
            $('#td_utilizacion').html(data.HorasUtilizacion);
            $('#td_disponibilidad').html(data.DisponibilidadTotal+'%');
            $('#td_mtbf').html(data.MTBF);
            $('#td_mttr').html(data.MTTR);
            $('#td_mantenimiento').html(data.CoeficienteMantenimiento+'%');

          
            $.each(data.AccionMantenimiento,function(key,item){
                addData(barChartAviso,key,item);
            });
            $.each(data.EstadoMaquina,function(key,item){
               addData(barChartCostoMantenimiento,key,item);
            });
            $.each(data.TipoServicio,function(key,item){
               addData(barChartAlcance,key,item);
            });
            $.each(data.OrigenFalla,function(key,item){
               addData(barChartOrigenFalla,key,item);
            });
            //console.log(Object.keys(data.AccionMantenimiento));
        });
    }

    
     function addSetData(chart, label, data){
        chart.data.labels.push(label);
        chart.data.datasets.forEach((dataset) => {
            dataset.data.push(data);
        });
        chart.update();
    }
    function addData(chart, label, data){
        chart.data.labels.push(label);
        chart.data.datasets.forEach((dataset) => {
            dataset.data.push(data);
        });
        chart.update();
    }

    function removeData(chart) {
        chart.data.labels = [];
        chart.data.datasets[0].data = [];
        chart.update();
    }
</script>
@endsection


@section('content')
<style type="text/css">
.content-wrapper {
    /*background: #89b0ec !important;*/
}
#tabla-info tr th,#tabla-info tr td{
    padding: 5px !important;
    font-size: 10px;
    color: #fff
}
#tabla-info tr th {
    border-bottom-color: red;
}
.nav-item.nav-profile.dropdown a.nav-link span{
    color: #fff !important;
}
.nav-item.nav-profile.dropdown a.nav-link::after{
    color: #fff !important;
}
.stretch-card{
    padding: 5px
}
label{
    color: #fff;
}
</style>

<div class="container-fluid">
    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <h4 style="color: #fff">Correctivo</h4>
        </div>
        <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2">
                    <div class="form-group">
                        <label class="title-input">Año</label>
                        <select id="fil_ano" class="form-control form-control-sm"  multiple="multiple">
                            <option value="2019" selected>2019</option>
                            <option value="2018">2018</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2">
                    <div class="form-group">
                        <label class="title-input">Estado Maquina</label>
                        <select id="fil_estado_maquina" class="form-control form-control-sm"  multiple="multiple">
                            <option selected value="1">Arriendo</option>
                            <option selected value="2">Propio</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2">
                    <div class="form-group">
                        <label class="title-input">Origen Falla</label>
                        <select id="fil_origen_falla" class="form-control form-control-sm"  multiple="multiple">
                            <option selected value="1">Equipo</option>
                            <option value="2">Operacion</option>
                            <option value="3">Mal Diagnostico</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2">
                    <div class="form-group">
                        <label class="title-input">Periodo</label>
                        <select id="fil_periodo" class="form-control form-control-sm"  multiple="multiple">
                            <option selected value="1" >1</option>
                            <option selected value="2" >2</option>
                            <option selected value="3" >3</option>
                            <option selected value="4" >4</option>
                            <option selected value="5" >5</option>
                            <option selected value="6" >6</option>
                            <option selected value="7" >7</option>
                            <option selected value="8" >8</option>
                            <option selected value="9" >9</option>
                            <option selected value="10" >10</option>
                            <option selected value="11" >11</option>
                            <option selected value="12" >12</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2">
                    <div class="form-group">
                        <label class="title-input">Cuentas</label>
                        <select id="fil_cuenta" class="form-control form-control-sm"  multiple="multiple">
                            @foreach($cuentas as $cuenta)
                            <option value="{{$cuenta->id_cuenta}}" selected>{{$cuenta->tx_nombre}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2">
                    <div class="form-group">
                        <label class="title-input">Tipo Maquina</label>
                        <select id="fil_tipo_maquina" class="form-control form-control-sm"  multiple="multiple">
                            @foreach($tipoMaquina as $tipo)
                            <option value="{{$tipo->id_maquina}}" selected>{{$tipo->tx_nombre}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
        </div>
        <div class="row">
            <table id="tabla-info" border="0" class="table">
                <thead>
                    <tr>
                        <th>Horas Detencion</th>
                        <th>Horas Totales:</th>
                        <th>Horas Utilizacion</th>
                        <th>Disponibilidad Total</th>
                        <th>MTTR</th>
                        <th>MTBF</th>
                        <th>Monto Arriendo</th>
                        <th>Monto Reparacion</th>
                        <th>Coeficiente de mantenimiento</th>
                    <tr>
                </thead>
                <tbody>
                    <tr>
                        <td id="td_hora_detencion" >0</td>
                        <td id="td_totales" >0</td>
                        <td id="td_utilizacion" >0</td>
                        <td id="td_disponibilidad" >0</td>
                        <td id="td_mttr" >0</td>
                        <td id="td_mtbf" >0</td>
                        <td id="td_arriendo" >0</td>
                        <td id="td_monto_reparacion" >0</td>
                        <td id="td_mantenimiento" >0</td>
                    <tr>
                </tbody>
            </table>    
        </div>
        <div class="row" style="margin-top: 20px">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6  stretch-card">
                <div class="card">
                    <div class="card-body" style="padding: 5px">
                        <canvas id="barChartAviso"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6  stretch-card">
                <div class="card">
                    <div class="card-body" style="padding: 5px">
                        <canvas id="barChartCostoMantenimiento"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6  stretch-card">
                <div class="card">
                    <div class="card-body" style="padding: 5px">
                        <canvas id="barChartAlcance"></canvas>
                    </div>
                </div>
            </div>     
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6  stretch-card">
                <div class="card">
                    <div class="card-body" style="padding: 5px">
                        <canvas id="barChartOrigenFalla"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection