@extends('layouts.app')

@section('script')

<script type="text/javascript">
(function($) {
  $.fn.inputFilter = function(inputFilter) {
    return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      }
    });
  };
}(jQuery));

	$(function() {

        $('.dp_fecha').datetimepicker({
		    format: 'L',
        	icons: {
		        time: 'fa fa-clock',
		        date: 'fa fa-calendar',
		        up: 'fa fa-chevron-up',
		        down: 'fa fa-chevron-down',
		        previous: 'fa fa-chevron-left',
		        next: 'fa fa-chevron-right',
		        today: 'fa fa-screenshot',
		        clear: 'fa fa-trash',
		        close: 'fa fa-remove'
		    }
        });
        $('.dp_mes').datetimepicker({
		    viewMode : 'months',
		    format : 'MM',
		    toolbarPlacement: "top",
		    //allowInputToggle: true,
		    icons: {
		        time: 'fa fa-time',
		        date: 'fa fa-calendar',
		        up: 'fa fa-chevron-up',
		        down: 'fa fa-chevron-down',
		        previous: 'fa fa-chevron-left',
		        next: 'fa fa-chevron-right',
		        today: 'fa fa-screenshot',
		        clear: 'fa fa-trash',
		        close: 'fa fa-remove'
		    }
		})

        $('.dp_hora').datetimepicker({format: 'HH:mm'});
	})

	$(".input_number").inputFilter(function(value) {
  		return /^\d*$/.test(value);
	});



	$('#form').submit(function(e){
		
		e.preventDefault();

		var formData = new FormData($('#form')[0]);
		
		$('#cssload').show();

		$.ajax({
			url:"{{route('saveForm')}}",
			type:"POST",
			data:formData,
			processData: false,
  			contentType: false,
  			dataType: "json",
		}).done(function(data){

			$('#cssload').hide();
			alert('Guardado correctamente');
			$('#form').trigger("reset");

		});

		return false;
	});

</script>
@endsection

@section('content')
<style type="text/css">
	.form-group {
	    margin-bottom: 0.5rem;
	}
	label {
	    display: inline-block;
	    margin-bottom: .1rem;
	}

</style>
<div class="container-fluid ">
    <div class="card" style="padding: 5px 10px">

    	<h3 style="text-align: center;">Formulario</h3>
    	<form id="form">
    		<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="form-group">
				<label for="periodo">Mes Periodo</label>
	            <input type="text" 
	            	id="numero_periodo" 
	            	name="numero_periodo" 
	            	class="form-control form-control-sm dp_mes datetimepicker-input" 
	            	data-target="#numero_periodo" data-toggle="datetimepicker" 
	            	placeholder="Numero periodo" 
	            	required="required">
			</div>
			<div class="form-group">
				<label for="fehca">Fecha</label>
	            <input 
	            	type="text" 
	            	id="fecha" 
	            	name="fecha" 
	            	class="form-control form-control-sm datetimepicker-input dp_fecha" 
	            	placeholder="Fecha" 
	            	data-target="#fecha" data-toggle="datetimepicker" 
	            	required="">
			</div>
			<div class="form-group">
				<label for="fehca">Numero iso tool</label>
	            <input data-provide="datepicker" type="text" id="iso_tool" name="iso_tool" class="form-control form-control-sm input_number" placeholder="Numero tool iso" required="">
			</div>
			<div class="form-group">
				<label for="numero_ot">Numero OT</label>
	            <input type="text" id="numero_ot" name="numero_ot" class="form-control form-control-sm input_number" placeholder="Numero OT" required="">
			</div>
			<div class="form-group">
				<label for="cuenta">Cuenta</label>
	            <select  class="form-control form-control-sm" required="required" name="cuenta">
	            	<option value="0">--Seleccionar Cuenta--</option>
	            	@foreach($cuentas as $key => $cuenta)
	            		<option value="{{$key}}">{{ $cuenta }}</option>
	            	@endforeach
	            </select>
			</div>
			<div class="form-group">
				<label for="tecnico">Tecnico</label>
	            <select class="form-control form-control-sm" required="required" name="tecnico">
	            	<option value="0">--Seleccionar Tecnico--</option>
	            	@foreach($tecnicos as $key => $tecnico)
	            		<option value="{{$key}}">{{ $tecnico }}</option>
	            	@endforeach
	            </select>
			</div>
			<div class="form-group">
				<label for="estado_maquina">Estado Maquina</label>
	            <select class="form-control form-control-sm" required="required" name="estado_maquina">
	            	<option value="0">--Seleccionar Maquina--</option>
	            	@foreach($EstadoMaquina as $key => $estado)
	            		<option value="{{$key}}">{{ $estado }}</option>
	            	@endforeach
	            </select>
			</div>
			<div class="form-group">
				<label for="inicio_trabajo">Inicio Trabajo</label>
	            <input type="text" 
	            	id="inicio_trabajo" 
	            	name="inicio_trabajo" 
	            	class="form-control form-control-sm dp_fecha datetimepicker-input" 
	            	placeholder="Inicio Trabajo"
	            	data-target="#inicio_trabajo" data-toggle="datetimepicker" 
	            	required="">
			</div>
			<div class="form-group">
				<label for="hora_inicio">Hora Inicio</label>
	            <input 
	            	type="text" 
	            	id="hora_inicio" 
	            	name="hora_inicio" 
	            	class="form-control form-control-sm dp_hora datetimepicker-input"
	            	data-target="#hora_inicio" data-toggle="datetimepicker" 
	            	placeholder="Hora Inicio" 
	            	required="">
			</div>
			<div class="form-group">
				<label for="fehca">Fin Trabajo</label>
	            <input 
	            	type="text" 
	            	id="fin_trabajo" 
	            	name="fin_trabajo" 
	            	class="form-control form-control-sm dp_fecha datetimepicker-input"
	            	data-target="#fin_trabajo" data-toggle="datetimepicker" 
	            	placeholder="Fin Trabajo" 
	            	required="">
			</div>
			<div class="form-group">
				<label for="fehca">Hora Fin</label>
	            <input 
	            	type="text" 
	            	id="hora_fin" 
	            	name="hora_fin" 
	            	class="form-control form-control-sm dp_hora datetimepicker-input"
	            	data-target="#hora_fin" data-toggle="datetimepicker" 
	            	placeholder="Hora Fin" 
	            	required="">
			</div>
			<div class="form-group">
				<label for="fehca">Dias Detencion</label>
	            <input 
	            	type="text" 
	            	id="dias_detencion" 
	            	name="dias_detencion" 
	            	class="form-control form-control-sm input_number" 
	            	placeholder="Dias Dentencion" 
	            	required="">
			</div>
			<div class="form-group">
				<label for="fehca">Hora Detencion</label>
	            <input 
	            	type="text" 
	            	id="horas_detencion" 
	            	name="horas_detencion" 
	            	class="form-control form-control-sm input_number"
	            	placeholder="Hora Detencion" 
	            	required="">
			</div>
			<div class="form-group">
				<label for="fehca">Aplica Gastos</label>
	            <select class="form-control form-control-sm" required="required" name="aplica_gasto">
	            	<option value="1">Si</option>
	            	<option value="2">No</option>
	            </select>
			</div>
			<div class="form-group">
				<label for="fehca">Numero Maquina</label>
	            <input 
	            	type="text" 
	            	id="numero_maquina" 
	            	name="numero_maquina" 
	            	class="form-control form-control-sm input_number" 
	            	placeholder="Numero Maquina" >
			</div>
			<div class="form-group">
				<label for="fehca">Tipo Maquina</label>
	            <select class="form-control form-control-sm" required="required" name="tipo_maquina">
	            	<option value="0">--Seleccionar Tipo--</option>
	            	@foreach($tipoMaquina as $key => $maquina)
	            		<option value="{{$key}}">{{ $maquina }}</option>
	            	@endforeach
	            </select>
			</div>
			<div class="form-group">
				<label for="fehca">Horometro</label>
	            <input 
	            	type="text" 
	            	id="horometro" 
	            	name="horometro" 
	            	class="form-control form-control-sm input_number" 
	            	placeholder="Horometro" >
			</div>
			<div class="form-group">
				<label for="fehca">Origen Falla</label>
             	<select class="form-control form-control-sm" required="required" name="origen_falla">
	            	<option value="0">--Seleccionar Origen Falla--</option>
	            	@foreach($OrigenFalla as $key => $falla)
	            		<option value="{{$key}}">{{ $falla }}</option>
	            	@endforeach
	            </select>
			</div>
			<div class="form-group">
				<label for="fehca">Aviso Segun</label>
	           	<select class="form-control form-control-sm" required="required" name="aviso_segun">
	            	<option value="0">--Seleccionar Aviso--</option>
	            	@foreach($AvisoSegun as $key => $aviso)
	            		<option value="{{$key}}">{{ $aviso }}</option>
	            	@endforeach
	            </select>
			</div>
			<div class="form-group">
				<label for="fehca">Tipo Servicio</label>
	           	<select class="form-control form-control-sm" required="required" name="tipo_servicio">
	            	<option value="0">--Seleccionar Tipo Servicio--</option>
	            	@foreach($TipoServicio as $key => $tipo)
	            		<option value="{{$key}}">{{ $tipo }}</option>
	            	@endforeach
	            </select>
			</div>
			<div class="form-group">
				<label for="fehca">Numero Presupuesto</label>
           		<input 
           			type="text" 
           			id="presupuesto" 
           			name="presupuesto" 
           			class="form-control form-control-sm input_number" 
           			placeholder="Presupuesto" >
			</div>
			<div class="form-group">
				<label for="fehca">Monto Reparacion</label>
	           	<input 
	           		type="text" 
	           		id="monto_reparacion" 
	           		name="monto_reparacion" 
	           		class="form-control form-control-sm input_number" 
	           		placeholder="Monto Reparacion" >
			</div>
			<div class="form-group">
				<label for="fehca">Monto Arriendo</label>
	           	<input 
	           		type="text" 
	           		id="monto_arriendo" 
	           		name="monto_arriendo" 
	           		class="form-control form-control-sm input_number" 
	           		placeholder="Monto Arriendo" >
			</div>
			<div class="form-group">
				<label for="observacion">Observacion</label>
				<textarea class="form-control" name="observacion"></textarea>
			</div>
			<button type="submit" class="btn btn-primary">Guardar</button>
		</form>

    </div>
</div>

<div id="cssload" class="cssload-container">
	<div class="cssload-whirlpool"></div>
</div>

<style type="text/css">


.cssload-container{
	position:fixed;
	top: 50%;
    left: 50%;
    z-index: 1000;
    display: none;
}
	
.cssload-whirlpool,
.cssload-whirlpool::before,
.cssload-whirlpool::after {
	position: absolute;
	top: 50%;
	left: 50%;
	border: 1px solid rgb(157,157,163);
	border-left-color: rgb(0,0,0);
	border-radius: 974px;
		-o-border-radius: 974px;
		-ms-border-radius: 974px;
		-webkit-border-radius: 974px;
		-moz-border-radius: 974px;
}

.cssload-whirlpool {
	margin: -24px 0 0 -24px;
	height: 49px;
	width: 49px;
	animation: cssload-rotate 1150ms linear infinite;
		-o-animation: cssload-rotate 1150ms linear infinite;
		-ms-animation: cssload-rotate 1150ms linear infinite;
		-webkit-animation: cssload-rotate 1150ms linear infinite;
		-moz-animation: cssload-rotate 1150ms linear infinite;
}

.cssload-whirlpool::before {
	content: "";
	margin: -22px 0 0 -22px;
	height: 43px;
	width: 43px;
	animation: cssload-rotate 1150ms linear infinite;
		-o-animation: cssload-rotate 1150ms linear infinite;
		-ms-animation: cssload-rotate 1150ms linear infinite;
		-webkit-animation: cssload-rotate 1150ms linear infinite;
		-moz-animation: cssload-rotate 1150ms linear infinite;
}

.cssload-whirlpool::after {
	content: "";
	margin: -28px 0 0 -28px;
	height: 55px;
	width: 55px;
	animation: cssload-rotate 2300ms linear infinite;
		-o-animation: cssload-rotate 2300ms linear infinite;
		-ms-animation: cssload-rotate 2300ms linear infinite;
		-webkit-animation: cssload-rotate 2300ms linear infinite;
		-moz-animation: cssload-rotate 2300ms linear infinite;
}



@keyframes cssload-rotate {
	100% {
		transform: rotate(360deg);
	}
}

@-o-keyframes cssload-rotate {
	100% {
		-o-transform: rotate(360deg);
	}
}

@-ms-keyframes cssload-rotate {
	100% {
		-ms-transform: rotate(360deg);
	}
}

@-webkit-keyframes cssload-rotate {
	100% {
		-webkit-transform: rotate(360deg);
	}
}

@-moz-keyframes cssload-rotate {
	100% {
		-moz-transform: rotate(360deg);
	}
}
</style>
@endsection